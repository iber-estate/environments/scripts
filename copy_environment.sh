#!/bin/bash

PROJECT_ROOT=/srv/iber
SOURCE_ENV=${1:-production}
DEST_ENV=${2}

if [[ -z ${DEST_ENV} ]]
then
    echo "Error: destination evironment is not defined"
    exit 1
fi

echo "Copy ${SOURCE_ENV} >> ${DEST_ENV}"
cd ${PROJECT_ROOT}/${SOURCE_ENV}
echo "Backup source DB"
docker compose exec db /bin/sh -c 'PGPASSWORD=${POSTGRES_PASSWORD} pg_dump --username=${POSTGRES_USER} --format=c ${POSTGRES_DB} -f /var/lib/postgresql/backup/copy.pgdump && chmod a+rw /var/lib/postgresql/backup/copy.pgdump'
mv ${PROJECT_ROOT}/${SOURCE_ENV}/postgresql/backup/copy.pgdump ${PROJECT_ROOT}/${DEST_ENV}/postgresql/backup/copy.pgdump
echo "Replace destination DB"
cd ${PROJECT_ROOT}/${DEST_ENV}
docker compose exec db /bin/sh -c 'PGPASSWORD=${POSTGRES_PASSWORD} pg_restore --clean --no-owner --role=${POSTGRES_USER} --username=${POSTGRES_USER} --format=c --dbname=${POSTGRES_DB} /var/lib/postgresql/backup/copy.pgdump'
echo "Sync files"
sudo rsync --archive --delete ${PROJECT_ROOT}/${SOURCE_ENV}/media/ ${PROJECT_ROOT}/${DEST_ENV}/media
sudo rsync --archive --delete ${PROJECT_ROOT}/${SOURCE_ENV}/static/ ${PROJECT_ROOT}/${DEST_ENV}/static
