#!/bin/sh

set -e

WDIR=$(dirname "$0")

export docker_compose_files=$(ls ${PROJECT_ROOT}/${CI_ENVIRONMENT_NAME}/docker-compose.d/*.yml)

j2 ${WDIR}/docker-compose.yml.j2 -o ${PROJECT_ROOT}/${CI_ENVIRONMENT_NAME}/docker-compose.yml
