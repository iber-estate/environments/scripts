#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import yaml
from pathlib import Path
import shutil
from argparse import ArgumentParser
import logging
from logging.handlers import RotatingFileHandler
import subprocess
from boto3 import client as S3Client
from datetime import datetime
import sys

workdir = Path(__file__).resolve().parent


current_time = datetime.now()

# Preparing arguments
argparser = ArgumentParser(description='backup PostgreSQL instances')
argparser.add_argument('--config',
                       help='path to config file',
                       type=Path,
                       required=True)
argparser.add_argument('--print',
                       help='print log to stdout',
                       action='store_true')
args = argparser.parse_args()

if args.print:
    logging.basicConfig(
        format='%(asctime)s [%(levelname)s] %(message)s',
        level=logging.INFO
    )
else:
    logging.basicConfig(
        format='%(asctime)s [%(levelname)s] %(message)s',
        level=logging.INFO,
        handlers=[
            RotatingFileHandler(
                workdir / 'main.log',
                maxBytes=104857600,
                backupCount=3
            )]
    )

with open(args.config, 'rt') as config_f:
    config = yaml.load(config_f, Loader=yaml.Loader)

tmp_dir = Path(config['postgresql']['tmp_dir'])
if not tmp_dir.is_dir():
    tmp_dir.mkdir()

s3 = S3Client(
    's3',
    aws_access_key_id=config['s3']['aws_access_key_id'],
    aws_secret_access_key=config['s3']['aws_secret_access_key'],
    region_name=config['s3']['region'],
    endpoint_url=config['s3']['endpoint']
)

for location in config['postgresql']['locations']:
    cmd = [
        'pg_dump',
        '--host={}'.format(location['host']),
        '--port={}'.format(location['port']),
        '--username={}'.format(location['user']),
        '--format=c'
    ]
    raw_fn = location['database'] + '.pgdump'
    compressed_fn = raw_fn + '.gz'
    logging.info('Dumping {}@{}'.format(
        location['database'], location['host']))
    # print(' '.join(cmd + [location['database']]))
    with open(tmp_dir / raw_fn, 'w') as raw_dump_file:
        pgsqldump = subprocess.Popen(
            cmd + [location['database']],
            env={'PGPASSWORD': location['password']},
            stdout=raw_dump_file,
            stderr=sys.stdout
        )
    pgsqldump.wait()
    if pgsqldump.returncode != 0:
        logging.error('dump failed with code {}'.format(pgsqldump.returncode))
        exit(pgsqldump.returncode)

    logging.info(f'Compressing {compressed_fn}')
    subprocess.run(
        ['gzip', '--force', raw_fn],
        cwd=tmp_dir,
        check=True
    )
    destination = "{}/{}/{}".format(
        config['postgresql']['folder'],
        current_time.strftime(config['s3']['folders_format']),
        compressed_fn
    )
    logging.info(f'Uploading {destination}')
    dump_file = tmp_dir / compressed_fn
    s3.upload_file(
        dump_file.as_posix(),
        config['s3']['bucket'],
        destination)
    dump_file.unlink()

if config['s3']['clean']:
    logging.info('Clean remote location')
    available_objects = s3.list_objects(
        Bucket=config['s3']['bucket'],
        Prefix=config['postgresql']['folder'] + '/'
    )
    folders = set()
    for obj in available_objects['Contents']:
        ts = obj['Key'].split('/')[-2]
        folders.add(ts)
    obsolete_folders = sorted(folders, reverse=False)[:-int(config['s3']['keep'])]
    obsolete_objects = set()
    for obj in available_objects['Contents']:
        ts = obj['Key'].split('/')[-2]
        if ts in obsolete_folders:
            obsolete_objects.add(obj['Key'])
    for obj in obsolete_objects:
        logging.info('Deleting {}'.format(obj))
        s3.delete_object(
            Bucket=config['s3']['bucket'],
            Key=obj
        )

logging.info('Backup FINISED')
