#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from configparser import ConfigParser
from pathlib import Path
from argparse import ArgumentParser
import psycopg2
import logging
from logging.handlers import RotatingFileHandler
import subprocess
from boto3 import client as S3Client
from datetime import datetime
import sys

workdir = Path(__file__).resolve().parent

config = ConfigParser()
config.read(workdir / 'config.ini')
current_time = datetime.now()

# Preparing arguments
argparser = ArgumentParser(description='backup PostgreSQL instances')
argparser.add_argument('--local',
                       help='perform local backup to provided directory',
                       type=str)
argparser.add_argument('--clean',
                       help='clean remote location',
                       action='store_true')
argparser.add_argument('--print',
                       help='print log to stdout',
                       action='store_true')
args = argparser.parse_args()

if args.print:
    logging.basicConfig(
        format='%(asctime)s [%(levelname)s] %(message)s',
        level=logging.INFO
    )
else:
    logging.basicConfig(
        format='%(asctime)s [%(levelname)s] %(message)s',
        level=logging.INFO,
        handlers=[
            RotatingFileHandler(
                workdir / 'main.log',
                maxBytes=104857600,
                backupCount=10
            )]
    )

logging.basicConfig(
    format='%(asctime)s [%(levelname)s] %(message)s',
    level=logging.INFO,
    handlers=[
        RotatingFileHandler(
            workdir / 'main.log',
            maxBytes=104857600,
            backupCount=10
        )]
)

blacklist = [
    'template0',
    'template1'
]
exclude_suffix = set()
exclude_prefix = set()

if config.get('general', 'exclude') is not None:
    for e in config.get('general', 'exclude').split(','):
        if e.startswith('*'):
            exclude_suffix.add(e.strip('*'))
        elif e.endswith('*'):
            exclude_prefix.add(e.strip('*'))
        else:
            blacklist.append(e)

s3 = S3Client(
    's3',
    aws_access_key_id=config['s3']['aws_access_key_id'],
    aws_secret_access_key=config['s3']['aws_secret_access_key'],
    region_name=config['s3']['region'],
    endpoint_url=config['s3']['endpoint']
)

db_connection = psycopg2.connect(
    host=config['postgresql']['host'],
    port=config['postgresql']['port'],
    user=config['postgresql']['user'],
    password=config['postgresql']['password'],
    database='postgres')
db_cursor = db_connection.cursor()
db_cursor.execute('SELECT datname FROM pg_database')
databases = set()
for d, in db_cursor.fetchall():
    if d not in blacklist:
        is_excluded = False
        for e in exclude_suffix:
            if d.endswith(e):
                is_excluded = True
                break
        for e in exclude_prefix:
            if d.startswith(e):
                is_excluded = True
                break
        if not is_excluded:
            databases.add(d)
db_cursor.close()
db_connection.close()

logging.info('found {} databases'.format(len(databases)))

cmd = [
    'pg_dump',
    '--host={}'.format(config['postgresql']['host']),
    '--port={}'.format(config['postgresql']['port']),
    '--username={}'.format(config['postgresql']['user']),
    '--format=c'
]

if args.local:
    tmp_dir = Path(args.local)
else:
    tmp_dir = Path(config['general']['tmp_dir'])


def gpg_encrypt(input_file: Path) -> Path:
    cmd = [
        "gpg", "--encrypt", "--batch", "--no-tty",
        "--recipient", config['general']['gpg_key'],
        input_file.as_posix()
    ]
    if (input_file.resolve().parent / f'{input_file.name}.gpg').exists():
        logging.warning(f'Removing already exists {input_file}')
        (input_file.resolve().parent / f'{input_file.name}.gpg').unlink()
    logging.info(f'Encrypting to {input_file}.gpg')
    subprocess.run(' '.join(cmd), shell=True, check=True)
    input_file.unlink()
    return input_file.resolve().parent / f'{input_file.name}.gpg'


for db in databases:
    raw_fn = db + '.pgdump'
    compressed_fn = raw_fn + config['general']['compressed_suffix']
    encrypted_fn = compressed_fn + '.gpg'
    for f in [raw_fn, compressed_fn]:
        if (tmp_dir / f).is_file():
            logging.warning(f'Already exist {f}, deleting')
            (tmp_dir / f).unlink()

    logging.info('Dumping {} to {}'.format(db, tmp_dir / raw_fn))
    with open(tmp_dir / raw_fn, 'w') as raw_dump_file:
        pgsqldump = subprocess.Popen(
            cmd + [db],
            env={'PGPASSWORD': config['postgresql']['password']},
            stdout=raw_dump_file,
            stderr=sys.stdout
        )
        pgsqldump.wait()
        if pgsqldump.returncode != 0:
            logging.error('dump failed with code {}'.format(pgsqldump.returncode))
            exit(pgsqldump.returncode)

    logging.info(f'Compressing {compressed_fn}')
    subprocess.run(
        [
            config['general']['compressor'],
            raw_fn
        ],
        cwd=tmp_dir,
        check=True
    )

    enc_path = gpg_encrypt(tmp_dir / compressed_fn)

    if not args.local:
        destination = config['s3']['folder'] \
            + '/' + current_time.strftime(config['general']['folders_format']) + '/' + encrypted_fn
        logging.info(f'Uploading {destination}')
        s3.upload_file(
            enc_path.as_posix(),
            config['s3']['bucket'],
            destination)
        enc_path.unlink()

if args.clean:
    logging.info('Clean remote location')
    available_objects = s3.list_objects(
        Bucket=config['s3']['bucket'],
        Prefix=config['s3']['folder'] + '/'
    )
    folders = set()
    for obj in available_objects['Contents']:
        ts = obj['Key'].split('/')[-2]
        folders.add(ts)
    obsolete_folders = sorted(folders, reverse=False)[:-config.getint('general', 'keep')]
    obsolete_objects = set()
    for obj in available_objects['Contents']:
        ts = obj['Key'].split('/')[-2]
        if ts in obsolete_folders:
            obsolete_objects.add(obj['Key'])
    for obj in obsolete_objects:
        logging.info('Deleting {}'.format(obj))
        s3.delete_object(
            Bucket=config['s3']['bucket'],
            Key=obj
        )

logging.info('Backup FINISED')
