#!/bin/bash

export PGPASSWORD=${POSTGRES_PASSWORD}

for db in $(psql --tuples-only --username=${POSTGRES_USER} <<< 'SELECT datname FROM pg_database'|grep -v template)
do
    # echo -n "Dumping ${db}..."
    pg_dump --username=${POSTGRES_USER} --format=c ${db} |gzip >/var/lib/postgresql/backup/${db}.pgdump.gz
    chmod a+rw /var/lib/postgresql/backup/${db}.pgdump.gz
    # echo "done"
done
