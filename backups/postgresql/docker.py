#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import docker
import yaml
from pathlib import Path
import shutil
from argparse import ArgumentParser
import logging
from logging.handlers import RotatingFileHandler
from boto3 import client as S3Client
from datetime import datetime
import sys

workdir = Path(__file__).resolve().parent

current_time = datetime.now()

# Preparing arguments
argparser = ArgumentParser(description='backup PostgreSQL instances')
argparser.add_argument('--config',
                       help='path to config file',
                       type=Path,
                       required=True)
argparser.add_argument('--print',
                       help='print log to stdout',
                       action='store_true')
args = argparser.parse_args()

if args.print:
    logging.basicConfig(
        format='%(asctime)s [%(levelname)s] %(message)s',
        level=logging.INFO
    )
else:
    logging.basicConfig(
        format='%(asctime)s [%(levelname)s] %(message)s',
        level=logging.INFO,
        handlers=[
            RotatingFileHandler(
                workdir / 'main.log',
                maxBytes=104857600,
                backupCount=3
            )]
    )

with open(args.config, 'rt') as config_f:
    config = yaml.load(config_f, Loader=yaml.Loader)

s3 = S3Client(
    's3',
    aws_access_key_id=config['s3']['aws_access_key_id'],
    aws_secret_access_key=config['s3']['aws_secret_access_key'],
    region_name=config['s3']['region'],
    endpoint_url=config['s3']['endpoint']
)

docker_c = docker.from_env()

for location in config['postgresql']:
    work_path = Path(location['path'])
    logging.info(f'Processing {work_path}')
    shutil.copy(workdir / 'backup.sh', work_path / 'backup.sh')
    container = docker_c.containers.get(location['container'])
    container.exec_run('bash /var/lib/postgresql/backup/backup.sh')
    for dump_file in work_path.glob('*.pgdump.gz'):
        destination = "{}/{}/{}".format(
            location['folder'],
            current_time.strftime(config['s3']['folders_format']),
            dump_file.name
        )
        logging.info(f'Uploading {destination}')
        s3.upload_file(
            dump_file.as_posix(),
            config['s3']['bucket'],
            destination)
        dump_file.unlink()
    if config['s3']['clean']:
        logging.info('Clean remote location')
        available_objects = s3.list_objects(
            Bucket=config['s3']['bucket'],
            Prefix=location['folder'] + '/'
        )
        folders = set()
        for obj in available_objects['Contents']:
            ts = obj['Key'].split('/')[-2]
            folders.add(ts)
        obsolete_folders = sorted(folders, reverse=False)[:-int(config['s3']['keep'])]
        obsolete_objects = set()
        for obj in available_objects['Contents']:
            ts = obj['Key'].split('/')[-2]
            if ts in obsolete_folders:
                obsolete_objects.add(obj['Key'])
        for obj in obsolete_objects:
            logging.info('Deleting {}'.format(obj))
            s3.delete_object(
                Bucket=config['s3']['bucket'],
                Key=obj
            )

logging.info('Backup FINISED')
