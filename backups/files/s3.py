#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import yaml
from pathlib import Path
import subprocess
from argparse import ArgumentParser
import logging
from logging.handlers import RotatingFileHandler
from boto3 import client as S3Client
from datetime import datetime
import sys

workdir = Path(__file__).resolve().parent

current_time = datetime.now()

# Preparing arguments
argparser = ArgumentParser(description='backup files')
argparser.add_argument('--config',
                       help='path to config file',
                       type=Path,
                       required=True)
argparser.add_argument('--print',
                       help='print log to stdout',
                       action='store_true')
args = argparser.parse_args()

if args.print:
    logging.basicConfig(
        format='%(asctime)s [%(levelname)s] %(message)s',
        level=logging.INFO
    )
else:
    logging.basicConfig(
        format='%(asctime)s [%(levelname)s] %(message)s',
        level=logging.INFO,
        handlers=[
            RotatingFileHandler(
                workdir / 'main.log',
                maxBytes=104857600,
                backupCount=3
            )]
    )

with open(args.config, 'rt') as config_f:
    config = yaml.load(config_f, Loader=yaml.Loader)

s3 = S3Client(
    's3',
    aws_access_key_id=config['s3']['aws_access_key_id'],
    aws_secret_access_key=config['s3']['aws_secret_access_key'],
    region_name=config['s3']['region'],
    endpoint_url=config['s3']['endpoint']
)

for archive in config['files']:
    work_path = Path(archive['workdir'])
    logging.info(f'Processing {archive["name"]}')
    arch_file = Path('/tmp') / archive["name"]
    cmd = [
        'tar', '-zcf', arch_file.as_posix()
    ] + archive['locations']
    subprocess.run(
        cmd,
        cwd=work_path,
        check=True
    )
    destination = "{}/{}/{}".format(
        archive['folder'],
        current_time.strftime(config['s3']['folders_format']),
        arch_file.name
    )
    logging.info(f'Uploading {destination}')
    s3.upload_file(
        arch_file.as_posix(),
        config['s3']['bucket'],
        destination)
    arch_file.unlink()
    if config['s3']['clean']:
        logging.info('Clean remote location')
        available_objects = s3.list_objects(
            Bucket=config['s3']['bucket'],
            Prefix=archive['folder'] + '/'
        )
        folders = set()
        for obj in available_objects['Contents']:
            ts = obj['Key'].split('/')[-2]
            folders.add(ts)
        obsolete_folders = sorted(folders, reverse=False)[:-int(config['s3']['keep'])]
        obsolete_objects = set()
        for obj in available_objects['Contents']:
            ts = obj['Key'].split('/')[-2]
            if ts in obsolete_folders:
                obsolete_objects.add(obj['Key'])
        for obj in obsolete_objects:
            logging.info('Deleting {}'.format(obj))
            s3.delete_object(
                Bucket=config['s3']['bucket'],
                Key=obj
            )

logging.info('Backup FINISED')
