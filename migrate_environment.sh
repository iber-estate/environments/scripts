#!/bin/bash

PROJECT_ROOT=/srv/iber
SOURCE_ENV=${1:-development}
DEST_HOST=oracle.envelop.is
DEST_PORT=25060

cd ${PROJECT_ROOT}/${SOURCE_ENV}
echo "Database..."
docker compose exec db /bin/sh -xc '''
export PGPASSWORD=${POSTGRES_PASSWORD}
pg_dump --username=${POSTGRES_USER} --format=c ${POSTGRES_DB} -f /tmp/migrate.pgdump
ls -lh /tmp/migrate.pgdump
pg_restore --clean --role=${POSTGRES_USER} --username=${POSTGRES_USER} --format=c --dbname=${POSTGRES_DB} --host=oracle.envelop.is --port=25060 /tmp/migrate.pgdump
'''
echo "Files..."
rsync --archive --delete ${PROJECT_ROOT}/${SOURCE_ENV}/media/ 128.140.62.208:${PROJECT_ROOT}/${SOURCE_ENV}/media
rsync --archive --delete ${PROJECT_ROOT}/${SOURCE_ENV}/static/ 128.140.62.208:${PROJECT_ROOT}/${SOURCE_ENV}/static
scp ${PROJECT_ROOT}/${SOURCE_ENV}/docker-compose.yml 128.140.62.208:${PROJECT_ROOT}/${SOURCE_ENV}/
scp -r ${PROJECT_ROOT}/${SOURCE_ENV}/docker-compose.d 128.140.62.208:${PROJECT_ROOT}/${SOURCE_ENV}/
scp ${PROJECT_ROOT}/${SOURCE_ENV}/.env 128.140.62.208:${PROJECT_ROOT}/${SOURCE_ENV}/
